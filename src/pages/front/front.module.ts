//import { NgModule } from '@angular/core';
//import { IonicPageModule } from 'ionic-angular';
//import { FrontPage } from './front';

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FrontPage } from './front';
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [
    FrontPage,
  ],
  imports: [
    IonicPageModule.forChild(FrontPage),
    ComponentsModule
  ],
  exports: [
    FrontPage,
  ]

})
export class FrontPageModule {}
