import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import {  Nav,NavController,IonicPage, LoadingController } from 'ionic-angular';
//import { IPostParams } from '../../interfaces/wordpress'

import { Store } from '@ngrx/store';
//import { Observable } from 'rxjs';

//import { excludePages } from '../../wp-config';
import { IPostParams, ICategory } from '../../interfaces/wordpress'
import { IAppState } from '../../interfaces/store'
import { WordpressProvider } from '../../providers/wordpress/wordpress';



export interface InterfacePage {
  ID          : string,
  title       : string,
  component   : any,
  params      : any,
  active?     : boolean
}


@IonicPage()

@Component({
  selector: 'page-front',
  templateUrl: 'front.html',
  providers:[ WordpressProvider ]
})
export class FrontPage {
  categories     : any;
  InterfacePage : any;

  @ViewChild(Nav) nav: Nav;
  @Output() setRootPage = new EventEmitter();

  categories: Array<InterfacePage>;


  search:IPostParams = {
    type: 'post'
  };
  constructor(public navCtrl: NavController,
              public wp: WordpressProvider,
              public loadingCtrl: LoadingController,
              public store:Store<IAppState>,) {

  }


  ngOnInit(){
    this.initializeMenu();
  }

  openPage(page:InterfacePage):void {
    this.setRootPage.emit(page);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FrontPage');
  }

  private initializeMenu(){
    this.categories = [];

    this.wp.getCategoryList()
        .subscribe(
            data => {
              Array.prototype.forEach.call(data, (cat:ICategory) => {
                if(cat.post_count > 0 && cat.parent == 0) {
                  this.categories.push({
                    ID   : cat.slug,
                    title: cat.name+'の情報を見る',
                    component: 'Category',
                    params: {
                      title: cat.name,
                      key  :cat.slug,
                    }
                  });
                  console.log('URL',cat.slug)
                }
              });
            },
            error => {
              console.log('errorだよ')
            }
        );
  }



}
